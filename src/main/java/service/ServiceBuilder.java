package service;

import android.support.annotation.NonNull;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.io.IOException;
import java.util.concurrent.TimeUnit;

import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import service.pref.PrefWrapper;

/**
 * Web services builder
 * Created by hungdn on 23/8/2017.
 */
public class ServiceBuilder {

  private static ServiceBuilder sInstance;
  private Retrofit mRetrofit;
  private ApiService mApiService;
  private String mBaseUrl;

  public ServiceBuilder(String endPoint) {
    mBaseUrl = endPoint;
  }

  public static ServiceBuilder getInstance() {
    return sInstance;
  }

  public static void init(String endPoint) {
    sInstance = new ServiceBuilder(endPoint);
  }

  private Retrofit getRetrofit(String endPoint) {
    HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor();
    interceptor.setLevel(HttpLoggingInterceptor.Level.BODY);
    OkHttpClient client = new OkHttpClient.Builder()
            .readTimeout(120, TimeUnit.SECONDS)
            .writeTimeout(120, TimeUnit.SECONDS)
            .connectTimeout(120, TimeUnit.SECONDS)
            .addInterceptor(interceptor)
            .addInterceptor(new Interceptor() {
              // User agent default
              @Override
              public Response intercept(@NonNull Chain chain) throws IOException {
                // Set original User agent
                Request original = chain.request();

                String token = PrefWrapper.getInstance().getUserSession();
                // Build request with headers
                Request request = original.newBuilder()
                        .header("isMobileApp", "1")
                        .header("Token", !StringUtils.isEmpty(token) ? token : "")
                        .method(original.method(), original.body())
                        .build();

                return chain.proceed(request);
              }
            })
            .build();

    if (mRetrofit == null) {

      Gson gson = new GsonBuilder()
//              .registerTypeAdapter(ResponseDTO.class, new UserDeserialier())
              .create();

      mRetrofit = new Retrofit.Builder()
              .baseUrl(endPoint)
              .client(client)
              .addConverterFactory(GsonConverterFactory.create(gson))
              .build();
    }

    return mRetrofit;
  }

  public ApiService getService() {
    if (mApiService == null) {
      mApiService = getRetrofit(mBaseUrl).create(ApiService.class);
    }

    return mApiService;
  }

  public ApiService getService(String endPoint) {
    if (mApiService == null) {
      mApiService = getRetrofit(endPoint).create(ApiService.class);
    }

    return mApiService;
  }
}
