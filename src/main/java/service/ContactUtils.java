package service;

import android.content.ContentUris;
import android.content.ContentValues;
import android.content.Context;
import android.net.Uri;
import android.provider.ContactsContract;

public class ContactUtils {

    public static void addContact(String name, String phone, Context activity) {

        Uri addContactsUri = ContactsContract.Data.CONTENT_URI;

        // Add an empty contact and get the generated id.
        long rowContactId = getRawContactId(activity);

        // Add contact name data.
        insertContactDisplayName(addContactsUri, rowContactId, name, activity);

        // Add contact phone data.
        insertContactPhoneNumber(addContactsUri, rowContactId, phone, activity);
    }

    // Insert newly created contact display name.
    private static void insertContactDisplayName(Uri addContactsUri, long rawContactId, String displayName, Context activity)
    {
        ContentValues contentValues = new ContentValues();

        contentValues.put(ContactsContract.Data.RAW_CONTACT_ID, rawContactId);

        // Each contact must has an mime type to avoid java.lang.IllegalArgumentException: mimetype is required error.
        contentValues.put(ContactsContract.Data.MIMETYPE, ContactsContract.CommonDataKinds.StructuredName.CONTENT_ITEM_TYPE);

        // Put contact display name value.
        contentValues.put(ContactsContract.CommonDataKinds.StructuredName.GIVEN_NAME, displayName);

        activity.getContentResolver().insert(addContactsUri, contentValues);

    }

    private static void insertContactPhoneNumber(Uri addContactsUri, long rawContactId, String phoneNumber, Context activity)
    {
        // Create a ContentValues object.
        ContentValues contentValues = new ContentValues();

        // Each contact must has an id to avoid java.lang.IllegalArgumentException: raw_contact_id is required error.
        contentValues.put(ContactsContract.Data.RAW_CONTACT_ID, rawContactId);

        // Each contact must has an mime type to avoid java.lang.IllegalArgumentException: mimetype is required error.
        contentValues.put(ContactsContract.Data.MIMETYPE, ContactsContract.CommonDataKinds.Phone.CONTENT_ITEM_TYPE);

        // Put phone number value.
        contentValues.put(ContactsContract.CommonDataKinds.Phone.NUMBER, phoneNumber);

        // Calculate phone type by user selection.
        int phoneContactType;

        phoneContactType = ContactsContract.CommonDataKinds.Phone.TYPE_WORK;
        // Put phone type value.
        contentValues.put(ContactsContract.CommonDataKinds.Phone.TYPE, phoneContactType);

        // Insert new contact data into phone contact list.
        activity.getContentResolver().insert(addContactsUri, contentValues);

    }

    // This method will only insert an empty data to RawContacts.CONTENT_URI
    // The purpose is to get a system generated raw contact id.
    private static long getRawContactId(Context activity) {
        // Insert an empty contact.
        ContentValues contentValues = new ContentValues();
        Uri rawContactUri = activity.getContentResolver().insert(ContactsContract.RawContacts.CONTENT_URI, contentValues);
        // Get the newly created contact raw id.
        return ContentUris.parseId(rawContactUri);
    }
}
