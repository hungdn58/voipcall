package service;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;
import service.dto.ResponseDTO;
import service.model.VoIPUser;

/**
 * Restful Services
 * Created by hungdn on 8/07/2016.
 */
public interface ApiService {
    @GET("voip/getUserInfo")
    Call<ResponseDTO<VoIPUser>> getUserInforFromExtension(@Query("channel") String channel);

    @GET("voip/registerChannel")
    Call<ResponseDTO<VoIPUser>> registerExtensionForVoipCall();

    @GET("voip/getUserForVoIP")
    Call<ResponseDTO<List<VoIPUser>>> getListUserByStation(@Query("station_id") int stationId);
}