package service;

public class StringUtils {
    private static String chuanHoa(String str) {
        str = str.trim();
        str = str.replaceAll("\\s+", " ");
        return str;
    }

    public static String chuanHoaDanhTuRieng(String str) {
        str = chuanHoa(str);
        String temp[] = str.split(" ");
        str = ""; // ? ^-^
        for (int i = 0; i < temp.length; i++) {
            str += String.valueOf(temp[i].charAt(0)).toUpperCase() + temp[i].substring(1);
            if (i < temp.length - 1) // ? ^-^
                str += " ";
        }
        return str;
    }

    public static boolean isEmpty(String msg) {
        return msg == null || msg.equalsIgnoreCase("");
    }

    public static String extractDigits(String src) {
        StringBuilder builder = new StringBuilder();
        for (int i = 0; i < src.length(); i++) {
            char c = src.charAt(i);
            if (Character.isDigit(c)) {
                builder.append(c);
            }
        }
        return builder.toString();
    }

    public static String getGroupNameFromGroupID(String groupId) {
        String groupName = "";
        for (GroupEnum groupEnum : GroupEnum.values()) {
            if (groupEnum.getGroupID().equalsIgnoreCase(groupId)) {
                groupName = groupEnum.getGroupName();
            }
        }

        return groupName;
    }
}
