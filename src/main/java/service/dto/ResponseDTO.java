package service.dto;

import com.google.gson.annotations.SerializedName;

import service.StringUtils;

/**
 * Common Response DTO for all API requests
 * Created by hungdn on 26/4/2018.
 */

public class ResponseDTO<T> {

  @SerializedName("message")
  private String message = "";
  @SerializedName("msg")
  private String msg = "";
  @SerializedName("data")
  private T data = null;
  @SerializedName("success")
  private Boolean success = false;

  /**
   *
   * @return
   * The message
   */
  public String getMessage() {
    return message;
  }

  public String getResponseMsg() {
      return StringUtils.isEmpty(message) ? msg : message;
  }

  /**
   *
   * @param message
   * The message
   */
  public void setMessage(String message) {
    this.message = message;
  }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public Boolean getSuccess() {
        return success;
    }

    public void setSuccess(Boolean success) {
        this.success = success;
    }

    /**
   *
   * @return
   * The data
   */
  public T getData() {
    return data;
  }

  /**
   *
   * @param data
   * The data
   */
  public void setData(T data) {
    this.data = data;
  }

}
