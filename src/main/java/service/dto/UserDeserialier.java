package service.dto;

import com.google.gson.Gson;
import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParseException;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;
import java.util.List;

import service.model.VoIPUser;

/**UserDeserialier
 *
 * Created by hungdn on 8/31/2017.
 */

public class UserDeserialier implements JsonDeserializer<ResponseDTO> {
  @Override
  public ResponseDTO deserialize(JsonElement json, Type typeOfT, JsonDeserializationContext context) throws JsonParseException {
    JsonObject jsonObject = json.getAsJsonObject();

    ResponseDTO responseDTO = new ResponseDTO<>();

    if (jsonObject.getAsJsonObject("data") == null) {
      responseDTO.setSuccess(jsonObject.get("success").getAsBoolean());
      responseDTO.setMessage(jsonObject.get("message").getAsString());

      TypeToken<List<VoIPUser>> token = new TypeToken<List<VoIPUser>>(){};
      List<VoIPUser> personList = new Gson().fromJson(jsonObject.getAsJsonArray("users"), token.getType());

      responseDTO.setData(personList);
    } else {
      responseDTO = new Gson().fromJson(json.toString(), ResponseDTO.class);
    }

    return responseDTO;
  }
}
