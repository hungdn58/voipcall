package service.pref;

import android.content.Context;
import android.content.SharedPreferences;

import com.google.gson.Gson;

import service.StringUtils;
import service.model.VoIPUser;

/**
 * Shared Preferences wrapper
 * Created by hungdn on 23/8/2017.
 */

public class PrefWrapper {
  private static PrefWrapper sInstance;

  public static final String MY_PREFERENCES = "Pref";
  public static final String KEY_USER = "user";
  public static final String KEY_VOIP_USER = "voip_user";

  private static Context mContext;

  private PrefWrapper(Context context) {
    mContext = context;
  }

  public static SharedPreferences getPreference() {
    return mContext.getSharedPreferences(MY_PREFERENCES, Context.MODE_PRIVATE);
  }

  public static void init(Context context) {
    sInstance = new PrefWrapper(context);
  }

  public static PrefWrapper getInstance() {
    return sInstance;
  }

  /**
   * Save User as json
   */
  public void saveUserSession(String userSession) {
    SharedPreferences.Editor editor = getPreference().edit();
    editor.putString(KEY_USER, userSession);
    editor.commit();
  }

  /**
   * Save VoipUser as json
   */
  public void saveVoipUser(VoIPUser voIPUser) {
    SharedPreferences.Editor editor = getPreference().edit();
    editor.putString(KEY_VOIP_USER, new Gson().toJson(voIPUser));
    editor.commit();
  }

  /**
   * Get User from saved json
   */
  public VoIPUser getVoipUser() {
    String userJson = getPreference().getString(KEY_VOIP_USER, null);
    if (StringUtils.isEmpty(userJson)) {
      return null;
    }

    return new Gson().fromJson(userJson, VoIPUser.class);
  }

  /**
   * Get User from saved json
   */
  public String getUserSession() {
    String userJson = getPreference().getString(KEY_USER, null);
    if (userJson == null) {
      return null;
    }

    return userJson;
  }

  /**
   * Remove setting by {@code key}
   */
  public void remove(String key) {
    getPreference().edit()
            .remove(key)
            .apply();
  }

}
