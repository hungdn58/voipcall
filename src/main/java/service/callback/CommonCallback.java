package service.callback;

import android.content.Context;

/**
 * Common callback for APIs
 * Created by hungdn on 11/7/2016.
 */

public class CommonCallback<T> extends BaseCallback<T> {
  private Context mContext;

  public CommonCallback(Context context) {
    mContext = context;
  }

  @Override
  public void onSuccess(T data) {
//    DialogUtils.dismissProgressDialog();
  }

  @Override
  public void onError(String errorCode, String errorMessage) {
    try {
      int statusCode = Integer.parseInt(errorCode);

//      DialogUtils.dismissProgressDialog();
      if (errorCode.equals("401")) {

      } else {
        String msg = "";
        if (statusCode == 0) {
          msg = "Vui lòng kiểm tra lại kết nối mạng";
        }
        else if (statusCode == 302 || statusCode == 403 || statusCode == 404){
          msg = "Không có quyền truy xuất dữ liệu (" + statusCode + ")";
        }

//        DialogUtils.showAlert(mContext, msg);
      }
    } catch (Exception e) {
//      DialogUtils.showAlert(mContext, errorCode);
    }


  }

  @Override
  public String getServerMsg() {
    return "";
  }
}
