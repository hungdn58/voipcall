package service;

public enum GroupEnum {

    SUPER_ADMIN("Administrator", "1")
    ,
    STATION_MANAGER("GĐCN", "2")
    ,
    HUMAN_RESOURCE_MANAGER("HCNS", "3")
    ,
    CUSTOMER_CARE("CSHK", "4")
    ,
    MODULE_MANAGER("Quản lý module", "5")
    ,
    DISTRIBUTOR("Điều phối", "6")
    ,
    CASHIER("Thu ngân", "7")
    ,
    ACCOUNTANT("Kế toán", "8")
    ,
    SALE("Sale", "9")
    ,
    SALE_B2C("Sale B2C", "10")
    ,
    TELESALE("Telesale", "11")
    ,
    CARRIER("COD", "12")
    ,
    GUEST("Khách", "13")
    ,
    COD_DISTRIBUTOR("COD kho vận", "14")
    ,
    ENGINEER("Kỹ thuật", "15")
    ,
    DRIVER("Tài xế xe tải", "16"),
    PK_NOW("PK Now", "17"),
    D_NOW("T NOW", "18"),
    POST_OFFIC("Bưu cục", "19"),
    DEVICE("Device", "20"),
    ONLY_CHECKIN("OnlyCheckin", "21");

    GroupEnum(String name, String groupId) {
        this.groupID = groupId;
        this.groupName = name;
    }

    private String groupName;
    private String groupID;

    public String getGroupName() {
        return groupName;
    }

    public void setGroupName(String groupName) {
        this.groupName = groupName;
    }

    public String getGroupID() {
        return groupID;
    }

    public void setGroupID(String groupID) {
        this.groupID = groupID;
    }
}
