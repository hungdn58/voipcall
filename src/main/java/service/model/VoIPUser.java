package service.model;

import com.google.gson.annotations.SerializedName;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by Admin on 20/01/2018.
 */

public class VoIPUser {

    @SerializedName("User")
    public SipUser sipUser;
    @SerializedName("VoipMap")
    public VoIPMap voIPMap;

}
