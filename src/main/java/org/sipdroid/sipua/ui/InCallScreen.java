package org.sipdroid.sipua.ui;

/*
 * Copyright (C) 2009 The Sipdroid Open Source Project
 *
 * This file is part of Sipdroid (http://www.sipdroid.org)
 *
 * Sipdroid is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This source code is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this source code; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

import android.annotation.SuppressLint;
import android.app.KeyguardManager;
import android.content.ContentResolver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.pm.ActivityInfo;
import android.content.res.Configuration;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.media.AudioManager;
import android.media.ToneGenerator;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.os.SystemClock;
import android.preference.PreferenceManager;
import android.provider.Settings;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.WindowManager;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Chronometer;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.ToggleButton;

import org.sipdroid.media.RtpStreamReceiver;
import org.sipdroid.media.RtpStreamSender;
import org.sipdroid.sipua.R;
import org.sipdroid.sipua.UserAgent;
import org.sipdroid.sipua.phone.Call;
import org.sipdroid.sipua.phone.CallCard;
import org.sipdroid.sipua.phone.CallerInfo;
import org.sipdroid.sipua.phone.Connection;
import org.sipdroid.sipua.phone.Phone;
import org.sipdroid.sipua.phone.PhoneUtils;

import service.ServiceBuilder;
import service.StringUtils;
import service.callback.CommonCallback;
import service.model.VoIPUser;

public class InCallScreen extends CallScreen implements View.OnClickListener, SensorEventListener,
        CallCard.OnCallActionEventListener {

  final int MSG_ANSWER = 1;
  final int MSG_ANSWER_SPEAKER = 2;
  final int MSG_BACK = 3;
  final int MSG_TICK = 4;
  final int MSG_POPUP = 5;
  final int MSG_ACCEPT = 6;
  final int MSG_ACCEPT_FORCE = 7;

  final int SCREEN_OFF_TIMEOUT = 12000;

  private boolean isRejectCall;

  private ImageView mAvatarIv;
  private TextView mCallStatusTv;
  private MarqueeTextView mCallerInforTv;
  private Chronometer mElapsedTimeTv;

  private LinearLayout mCancelCallLl;
  private LinearLayout mAnswerCallLl;
  private LinearLayout mSpeakerLl;
  private LinearLayout mVolumnLl;
  private LinearLayout mHoldingCallLl;
  private View mBottomIncallAction;
  private RelativeLayout mEndCallRl;
  private TextView mErrorText;

  private ToggleButton mSpeakerTb;
  private ToggleButton mVolumnTb;
  private ToggleButton mHoldingTb;

  private LinearLayout mIncallControlsContainerLl;
  private RelativeLayout mIncallAnswerContainerLl;

  private static final String LOG_TAG = "PHONE/CallCard";
  private static final boolean DBG = false;

  Phone ccPhone;
  int oldtimeout;
  SensorManager sensorManager;
  Sensor proximitySensor;
  boolean first;

  void screenOff(boolean off) {
    ContentResolver cr = getContentResolver();

    if (proximitySensor != null)
      return;
    if (off) {
      if (oldtimeout == 0) {
        oldtimeout = Settings.System.getInt(cr, Settings.System.SCREEN_OFF_TIMEOUT, 60000);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
          if (Settings.System.canWrite(InCallScreen.this)) {
            Settings.System.putInt(cr, Settings.System.SCREEN_OFF_TIMEOUT, SCREEN_OFF_TIMEOUT);
          }
        }
      }
    } else {
      if (oldtimeout == 0 && Settings.System.getInt(cr, Settings.System.SCREEN_OFF_TIMEOUT, 60000) == SCREEN_OFF_TIMEOUT)
        oldtimeout = 60000;
      if (oldtimeout != 0) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
          if (Settings.System.canWrite(InCallScreen.this)) {
            Settings.System.putInt(cr, Settings.System.SCREEN_OFF_TIMEOUT, oldtimeout);
          }
        }
        oldtimeout = 0;
      }
    }
  }

  @Override
  public void onStop() {
    super.onStop();
    mHandler.removeMessages(MSG_BACK);
    mHandler.removeMessages(MSG_ACCEPT);
    mHandler.sendEmptyMessageDelayed(MSG_ACCEPT_FORCE, 1000);
    if (Receiver.call_state == UserAgent.UA_STATE_IDLE)
      finish();
    sensorManager.unregisterListener(this);
    started = false;

    if (Integer.parseInt(Build.VERSION.SDK) < 5 || Integer.parseInt(Build.VERSION.SDK) > 7)
      reenableKeyguard();
  }

  @Override
  public void onStart() {
    super.onStart();
    mHandler.removeMessages(MSG_ACCEPT_FORCE);
    if (Receiver.call_state == UserAgent.UA_STATE_IDLE)
      mHandler.sendEmptyMessageDelayed(MSG_BACK, Receiver.call_end_reason == -1 ?
              2000 : 5000);
    first = true;
    pactive = false;
    pactivetime = SystemClock.elapsedRealtime();
    sensorManager.registerListener(this, proximitySensor, SensorManager.SENSOR_DELAY_NORMAL);
    started = true;

    if (Integer.parseInt(Build.VERSION.SDK) < 5 || Integer.parseInt(Build.VERSION.SDK) > 7)
      disableKeyguard();
  }

  @Override
  public void onPause() {
    super.onPause();
    if (!Sipdroid.release) Log.i("SipUA:", "on pause");
    switch (Receiver.call_state) {
      case UserAgent.UA_STATE_INCOMING_CALL:
//    		if (!RtpStreamReceiver.isBluetoothAvailable()) Receiver.moveTop();
        break;
      case UserAgent.UA_STATE_IDLE:
        if (Receiver.ccCall != null)
//          mCallCard.displayMainCallStatus(ccPhone, Receiver.ccCall);
        mHandler.sendEmptyMessageDelayed(MSG_BACK, Receiver.call_end_reason == -1 ?
                2000 : 5000);
        break;
    }
    if (t != null) {
      running = false;
      t.interrupt();
    }
    screenOff(false);
    if (Integer.parseInt(Build.VERSION.SDK) >= 5 && Integer.parseInt(Build.VERSION.SDK) <= 7)
      reenableKeyguard();
		if (mElapsedTimeTv != null) mElapsedTimeTv.stop();
  }

  void moveBack() {
    if (Receiver.ccConn != null && !isRejectCall) {
      // after an outgoing call don't fall back to the contact
      // or call log because it is too easy to dial accidentally from there
//      startActivity(Receiver.createHomeIntent());
      mErrorText.setText(getString(R.string.call_error));
      setVisibleWithFade(mErrorText, true);
      isRejectCall = false;

    }

    mIncallAnswerContainerLl.setVisibility(View.GONE);
    mIncallControlsContainerLl.setVisibility(View.GONE);
    mElapsedTimeTv.stop();

    new Handler().postDelayed(new Runnable() {
      @Override
      public void run() {
//            mIncallControlsContainerLl.setVisibility(View.GONE);
        finish();
      }
    }, 3000);
//    onStop();
  }

  Context mContext = this;

  long enabletime;
  KeyguardManager mKeyguardManager;
  KeyguardManager.KeyguardLock mKeyguardLock;
  boolean enabled;

  void disableKeyguard() {
    if (mKeyguardManager == null) {
      mKeyguardManager = (KeyguardManager) getSystemService(Context.KEYGUARD_SERVICE);
      mKeyguardLock = mKeyguardManager.newKeyguardLock("Sipdroid");
      enabled = true;
    }
    if (enabled) {
      mKeyguardLock.disableKeyguard();
      if (Integer.parseInt(Build.VERSION.SDK) == 16 && Build.MODEL.contains("HTC One"))
        mKeyguardManager.exitKeyguardSecurely(new KeyguardManager.OnKeyguardExitResult() {
          public void onKeyguardExitResult(boolean success) {
          }
        });
      enabled = false;
      enabletime = SystemClock.elapsedRealtime();
    }
  }

  void reenableKeyguard() {
    if (!enabled) {
      try {
        if (Integer.parseInt(Build.VERSION.SDK) < 5)
          Thread.sleep(1000);
      } catch (InterruptedException e) {
      }
      mKeyguardLock.reenableKeyguard();
      enabled = true;
    }
  }

  @Override
  public void onResume() {
    super.onResume();

    if (Integer.parseInt(Build.VERSION.SDK) >= 5 && Integer.parseInt(Build.VERSION.SDK) <= 7)
      disableKeyguard();
    String callId;

    if (!Sipdroid.release) Log.i("SipUA:", "on resume");
    callId = getIntent().getStringExtra("caller_id");

    updateRemoteName(callId);

    switch (Receiver.call_state) {

      case UserAgent.UA_STATE_INCOMING_CALL:

        if (Receiver.pstn_state == null || Receiver.pstn_state.equals("IDLE"))
          if (PreferenceManager.getDefaultSharedPreferences(mContext).getBoolean(org.sipdroid.sipua.ui.Settings.PREF_AUTO_ON, org.sipdroid.sipua.ui.Settings.DEFAULT_AUTO_ON) &&
                  !mKeyguardManager.inKeyguardRestrictedInputMode())
            mHandler.sendEmptyMessageDelayed(MSG_ANSWER, 1000);
          else if ((PreferenceManager.getDefaultSharedPreferences(mContext).getBoolean(org.sipdroid.sipua.ui.Settings.PREF_AUTO_ONDEMAND, org.sipdroid.sipua.ui.Settings.DEFAULT_AUTO_ONDEMAND) &&
                  PreferenceManager.getDefaultSharedPreferences(mContext).getBoolean(org.sipdroid.sipua.ui.Settings.PREF_AUTO_DEMAND, org.sipdroid.sipua.ui.Settings.DEFAULT_AUTO_DEMAND)) ||
                  (PreferenceManager.getDefaultSharedPreferences(mContext).getBoolean(org.sipdroid.sipua.ui.Settings.PREF_AUTO_HEADSET, org.sipdroid.sipua.ui.Settings.DEFAULT_AUTO_HEADSET) &&
                          Receiver.headset > 0))
            mHandler.sendEmptyMessageDelayed(MSG_ANSWER_SPEAKER, 10000);
        break;
      case UserAgent.UA_STATE_INCALL:
//        mDialerDrawer.close();
//        mDialerDrawer.setVisibility(View.VISIBLE);
        displayMainCallStatus(ccPhone, Receiver.ccCall);

        if (Receiver.docked <= 0)
          screenOff(true);
        break;
      case UserAgent.UA_STATE_IDLE:
        if (!mHandler.hasMessages(MSG_BACK))

          displayMainCallStatus(ccPhone, Receiver.ccCall);
          moveBack();
        break;
      case UserAgent.UA_STATE_OUTGOING_CALL:

        displayMainCallStatus(ccPhone, Receiver.ccCall);

        break;
    }

    updateCallStateBar(Receiver.ccCall);

    if (Receiver.call_state != UserAgent.UA_STATE_INCALL) {
//      mDialerDrawer.close();
//      mDialerDrawer.setVisibility(View.GONE);
    }
//    if (Receiver.ccCall != null) mCallCard.displayMainCallStatus(ccPhone, Receiver.ccCall);
//    if (mSlidingCardManager != null) mSlidingCardManager.showPopup();
    mHandler.sendEmptyMessage(MSG_TICK);
    mHandler.sendEmptyMessage(MSG_POPUP);
    if (t == null && Receiver.call_state != UserAgent.UA_STATE_IDLE) {
//      mDigits.setText("");
      running = true;
      (t = new Thread() {
        public void run() {
          int len = 0;
          long time;
          ToneGenerator tg = null;

          if (Settings.System.getInt(getContentResolver(),
                  Settings.System.DTMF_TONE_WHEN_DIALING, 1) == 1)
            tg = new ToneGenerator(AudioManager.STREAM_VOICE_CALL, (int) (ToneGenerator.MAX_VOLUME * 2 * org.sipdroid.sipua.ui.Settings.getEarGain()));
          for (; ; ) {
            if (!running) {
              t = null;
              break;
            }
//            if (len != mDigits.getText().length()) {
//              time = SystemClock.elapsedRealtime();
//              if (tg != null) tg.startTone(mToneMap.get(mDigits.getText().charAt(len)));
//              Receiver.engine(Receiver.mContext).info(mDigits.getText().charAt(len++), 250);
//              time = 250 - (SystemClock.elapsedRealtime() - time);
//              try {
//                if (time > 0) sleep(time);
//              } catch (InterruptedException e) {
//              }
//              if (tg != null) tg.stopTone();
//              try {
//                if (running) sleep(250);
//              } catch (InterruptedException e) {
//              }
//              continue;
//            }
            mHandler.sendEmptyMessage(MSG_TICK);
            try {
              sleep(1000);
            } catch (InterruptedException e) {
            }
          }
          if (tg != null) tg.release();
        }
      }).start();
    }
  }

  private void updateCallStateBar(Call ccCall) {
    Call.State state = ccCall.getState();
    int stateText = -1;

    switch (state) {
      case ACTIVE:
        showCallConnected();

        mIncallControlsContainerLl.setVisibility(View.VISIBLE);
        mIncallAnswerContainerLl.setVisibility(View.GONE);

        if (DBG) log("displayMainCallStatus: start periodicUpdateTimer");
        break;

      case HOLDING:
        showCallOnhold();

        stateText = R.string.on_hold;
        break;

      case DISCONNECTED:
        reset();
        showCallEnded();

        stateText = R.string.call_state_disconnected;
        mIncallControlsContainerLl.setVisibility(View.GONE);
        mElapsedTimeTv.stop();

        new Handler().postDelayed(new Runnable() {
          @Override
          public void run() {
            finish();
          }
        }, 3000);

        break;

      case DIALING:
      case ALERTING:
        showCallConnecting();

        stateText = R.string.call_state_calling;

        break;

      case INCOMING:
      case WAITING:
        showCallIncoming();

        stateText = R.string.call_state_incoming;

        break;

      case IDLE:
        // The "main CallCard" should never display an idle call!
        Log.w(LOG_TAG, "displayMainCallStatus: IDLE call in the main call card!");
        break;

      default:
        Log.w(LOG_TAG, "displayMainCallStatus: unexpected call state: " + state);
        break;
    }

    if(stateText != -1) {
      mCallStatusTv.setText(stateText);
      setVisibleWithFade(mCallStatusTv, true);
    } else {
      setVisibleWithFade(mCallStatusTv, false);
    }
  }

  private void setVisibleWithFade(View v, boolean in) {
    if(v.getVisibility() == View.VISIBLE && in) {
      // Already visible and ask to show, ignore
      return;
    }
    if(v.getVisibility() == View.GONE && !in) {
      // Already gone and ask to hide, ignore
      return;
    }

    Animation anim = AnimationUtils.loadAnimation(mContext, in ? android.R.anim.fade_in : android.R.anim.fade_out);
    anim.setDuration(1000);
    v.startAnimation(anim);
    v.setVisibility(in ? View.VISIBLE : View.GONE);
  }

  private void updateRemoteName(String callId) {
    ServiceBuilder.getInstance().getService().getUserInforFromExtension(callId).enqueue(new CommonCallback<VoIPUser>(mContext) {
      @Override
      public void onSuccess(VoIPUser data) {
        super.onSuccess(data);

        Log.e("@@@@@", data.sipUser.fullname);

        bindCallerInfor(data);
      }

    });
  }

  private void bindCallerInfor(VoIPUser data) {
    String groupName = StringUtils.getGroupNameFromGroupID(data.sipUser.group_id);
    mCallerInforTv.setText(String.format(getString(R.string.voip_name), data.sipUser.fullname, groupName));
  }

  @SuppressLint("HandlerLeak")
  Handler mHandler = new Handler() {
    @SuppressLint("SetTextI18n")
    public void handleMessage(Message msg) {
      switch (msg.what) {
        case MSG_ANSWER:
          if (Receiver.call_state == UserAgent.UA_STATE_INCOMING_CALL)
            answer();
//          Receiver.engine(mContext).speaker(AudioManager.MODE_IN_CALL);
          break;
        case MSG_ANSWER_SPEAKER:
          if (Receiver.call_state == UserAgent.UA_STATE_INCOMING_CALL) {
            answer();
            Receiver.engine(mContext).speaker(AudioManager.MODE_NORMAL);
          }
          break;
        case MSG_BACK:
          moveBack();
          break;
        case MSG_TICK:
//          mCodec.setText(RtpStreamReceiver.getCodec());
          if (RtpStreamReceiver.good != 0) {

            AudioManager am = (AudioManager) Receiver.mContext.getSystemService(Context.AUDIO_SERVICE);
            int maxVolume = am.getStreamMaxVolume(AudioManager.STREAM_MUSIC);

            am.setStreamVolume(AudioManager.STREAM_MUSIC, maxVolume, 0);

            if (RtpStreamReceiver.timeout != 0)
              Log.e("@@@@@","no data");
            else if (RtpStreamSender.m > 1)
              Log.e("@@@@@", Math.round(RtpStreamReceiver.loss / RtpStreamReceiver.good * 100) + "%loss, " +
                      Math.round(RtpStreamReceiver.lost / RtpStreamReceiver.good * 100) + "%lost, " +
                      Math.round(RtpStreamReceiver.late / RtpStreamReceiver.good * 100) + "%late (>" +
                      (RtpStreamReceiver.jitter - 250 * RtpStreamReceiver.mu) / 8 / RtpStreamReceiver.mu + "ms)");
            else
              Log.e("@@@@@",Math.round(RtpStreamReceiver.lost / RtpStreamReceiver.good * 100) + "%lost, " +
                      Math.round(RtpStreamReceiver.late / RtpStreamReceiver.good * 100) + "%late (>" +
                      (RtpStreamReceiver.jitter - 250 * RtpStreamReceiver.mu) / 8 / RtpStreamReceiver.mu + "ms)");
          }
          break;
        case MSG_POPUP:
//          if (mSlidingCardManager != null) mSlidingCardManager.showPopup();
          break;
        case MSG_ACCEPT:
        case MSG_ACCEPT_FORCE:
          setScreenBacklight((float) -1);
          getWindow().setFlags(0,
                  WindowManager.LayoutParams.FLAG_FULLSCREEN);
//          if (mDialerDrawer != null) {
//            mDialerDrawer.close();
//            mDialerDrawer.setVisibility(View.VISIBLE);
//          }
          ContentResolver cr = getContentResolver();
          if (hapticset) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
              if (Settings.System.canWrite(InCallScreen.this)) {
                Settings.System.putInt(cr, Settings.System.HAPTIC_FEEDBACK_ENABLED, haptic);
              }
            }
            hapticset = false;
          }
          break;
      }
    }
  };

  private void showMessage(){
    AlertDialog.Builder builder = new AlertDialog.Builder(mContext);
    builder.setTitle("");
    builder.setMessage("Action đang cập nhật");
    builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
      @Override
      public void onClick(DialogInterface dialog, int which) {

      }
    });
    builder.create().show();
  }

  Thread t;
//  EditText mDigits;
  boolean running;
  public static boolean started;

  @Override
  public void onCreate(Bundle icicle) {
    setTheme(R.style.AppTheme_Dialog);

    super.onCreate(icicle);

    setContentView(R.layout.incall);

    mAvatarIv = (ImageView) findViewById(R.id.avatar_iv);
    mCallStatusTv = (TextView) findViewById(R.id.call_status_text);
    mCallerInforTv = (MarqueeTextView) findViewById(R.id.contact_name_display_name);
    mElapsedTimeTv = (Chronometer) findViewById(R.id.elapsedTime);
    mBottomIncallAction = findViewById(R.id.bottom_incall_action_ll);

    mCancelCallLl = (LinearLayout) findViewById(R.id.cancel_bt);
    mAnswerCallLl = (LinearLayout) findViewById(R.id.answer_ll);
    mErrorText = (TextView) findViewById(R.id.call_error_tv);

    mSpeakerLl = (LinearLayout) findViewById(R.id.speaker_ll);
    mVolumnLl = (LinearLayout) findViewById(R.id.voice_ll);
    mHoldingCallLl = (LinearLayout) findViewById(R.id.holding_ll);

    mEndCallRl = (RelativeLayout) findViewById(R.id.endButton);

    mIncallControlsContainerLl = (LinearLayout) findViewById(R.id.incall_controls_container_ll);
    mIncallAnswerContainerLl = (RelativeLayout) findViewById(R.id.incall_answer_ll);

    mSpeakerTb = (ToggleButton) findViewById(R.id.speaker_iv);
    mSpeakerTb.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
      @Override
      public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
//        Receiver.engine(mContext).speaker(RtpStreamReceiver.speakermode == AudioManager.MODE_NORMAL?
//                AudioManager.MODE_IN_CALL: AudioManager.MODE_NORMAL);
      }
    });

    mVolumnTb = (ToggleButton) findViewById(R.id.voice_iv);
    mVolumnTb.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
      @Override
      public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
        Receiver.engine(mContext).togglemute();
      }
    });

    mHoldingTb = (ToggleButton) findViewById(R.id.holding_iv);
    mHoldingTb.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
      @Override
      public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
        Receiver.engine(mContext).togglehold();
      }
    });

    mCancelCallLl.setOnClickListener(this);
    mAnswerCallLl.setOnClickListener(this);
    mSpeakerLl.setOnClickListener(this);
    mVolumnLl.setOnClickListener(this);
    mHoldingCallLl.setOnClickListener(this);
    mEndCallRl.setOnClickListener(this);

    sensorManager = (SensorManager) getSystemService(SENSOR_SERVICE);
    proximitySensor = sensorManager.getDefaultSensor(Sensor.TYPE_PROXIMITY);

    if (!android.os.Build.BRAND.equalsIgnoreCase("archos"))
      setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_NOSENSOR);
  }

  public void reject() {
    if (Receiver.ccCall != null) {
      Receiver.stopRingtone();
      Receiver.ccCall.setState(Call.State.DISCONNECTED);
      displayMainCallStatus(ccPhone, Receiver.ccCall);
//      mDialerDrawer.close();
//      mDialerDrawer.setVisibility(View.GONE);
//      if (mSlidingCardManager != null)
//        mSlidingCardManager.showPopup();
    }
    (new Thread() {
      public void run() {
        Receiver.engine(mContext).rejectcall();
      }
    }).start();
  }

  public void endCall() {
    if (Receiver.ccCall != null) {
      Receiver.stopRingtone();
      Receiver.ccCall.setState(Call.State.END_CALL);
      displayMainCallStatus(ccPhone, Receiver.ccCall);
//      mDialerDrawer.close();
//      mDialerDrawer.setVisibility(View.GONE);
//      if (mSlidingCardManager != null)
//        mSlidingCardManager.showPopup();
    }
    (new Thread() {
      public void run() {
        Receiver.engine(mContext).rejectcall();
      }
    }).start();
  }

  public void answer() {
    (new Thread() {
      public void run() {
        Receiver.engine(mContext).answercall();
      }
    }).start();
    if (Receiver.ccCall != null) {
      Receiver.ccCall.setState(Call.State.ACTIVE);
      Receiver.ccCall.base = SystemClock.elapsedRealtime();

      displayMainCallStatus(ccPhone, Receiver.ccCall);
//			mDialerDrawer.setVisibility(View.VISIBLE);
//	        if (mSlidingCardManager != null)
//	        	mSlidingCardManager.showPopup();
    }
  }

  @Override
  public boolean onKeyDown(int keyCode, KeyEvent event) {
    switch (keyCode) {
      case KeyEvent.KEYCODE_MENU:
        if (Receiver.call_state == UserAgent.UA_STATE_INCOMING_CALL) {
          answer();
          return true;
        }
        break;

      case KeyEvent.KEYCODE_CALL:
        switch (Receiver.call_state) {
          case UserAgent.UA_STATE_INCOMING_CALL:
            answer();
            break;
          case UserAgent.UA_STATE_INCALL:
          case UserAgent.UA_STATE_HOLD:
            Receiver.engine(this).togglehold();
            break;
        }
        // consume KEYCODE_CALL so PhoneWindow doesn't do anything with it
        return true;

      case KeyEvent.KEYCODE_BACK:
//        if (mDialerDrawer.isOpened())
//          mDialerDrawer.animateClose();
        return true;

      case KeyEvent.KEYCODE_CAMERA:
        // Disable the CAMERA button while in-call since it's too
        // easy to press accidentally.
        return true;

      case KeyEvent.KEYCODE_VOLUME_DOWN:
      case KeyEvent.KEYCODE_VOLUME_UP:
        if (Receiver.call_state == UserAgent.UA_STATE_INCOMING_CALL) {
          Receiver.stopRingtone();
          return true;
        }
        RtpStreamReceiver.adjust(keyCode, true, !(pactive || SystemClock.elapsedRealtime() - pactivetime < 1000));
        return true;
    }
    if (Receiver.call_state == UserAgent.UA_STATE_INCALL) {
      char number = event.getNumber();
      if (Character.isDigit(number) || number == '*' || number == '#') {
//        appendDigit(number);
        return true;
      }
    }
    return super.onKeyDown(keyCode, event);
  }

  @Override
  public boolean onKeyUp(int keyCode, KeyEvent event) {
    switch (keyCode) {
      case KeyEvent.KEYCODE_VOLUME_DOWN:
      case KeyEvent.KEYCODE_VOLUME_UP:
        RtpStreamReceiver.adjust(keyCode, false, !(pactive || SystemClock.elapsedRealtime() - pactivetime < 1000));
        return true;
      case KeyEvent.KEYCODE_ENDCALL:
        if (Receiver.pstn_state == null ||
                (Receiver.pstn_state.equals("IDLE") && (SystemClock.elapsedRealtime() - Receiver.pstn_time) > 3000)) {
          reject();
          return true;
        }
        break;
    }
    Receiver.pstn_time = 0;
    return false;
  }

  @Override
  public void onAccuracyChanged(Sensor sensor, int accuracy) {
  }

  void setScreenBacklight(float a) {
    WindowManager.LayoutParams lp = getWindow().getAttributes();
    lp.screenBrightness = a;
    getWindow().setAttributes(lp);
  }

  static final float PROXIMITY_THRESHOLD = 5.0f;
  public static boolean pactive;
  public static long pactivetime;
  static int haptic;
  static boolean hapticset;

  @Override
  public void onSensorChanged(SensorEvent event) {
    boolean keepon = PreferenceManager.getDefaultSharedPreferences(mContext).getBoolean(org.sipdroid.sipua.ui.Settings.PREF_KEEPON, org.sipdroid.sipua.ui.Settings.DEFAULT_KEEPON);
    if (first) {
      first = false;
      return;
    }
    float distance = event.values[0];
    boolean active = (distance >= 0.0 && distance < PROXIMITY_THRESHOLD && distance < event.sensor.getMaximumRange());
    if (!keepon ||
            Receiver.call_state == UserAgent.UA_STATE_HOLD)
      active = false;
    pactive = active;
    pactivetime = SystemClock.elapsedRealtime();
    if (!active) {
      mHandler.sendEmptyMessageDelayed(MSG_ACCEPT, 1000);
      return;
    }
    mHandler.removeMessages(MSG_ACCEPT);
    setScreenBacklight((float) 0.1);
    getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
            WindowManager.LayoutParams.FLAG_FULLSCREEN);
    closeOptionsMenu();
//    mDialerDrawer.close();
//    mDialerDrawer.setVisibility(View.GONE);
    ContentResolver cr = getContentResolver();
    if (!hapticset) {
      haptic = Settings.System.getInt(cr, Settings.System.HAPTIC_FEEDBACK_ENABLED, 1);
      hapticset = true;
    }
    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
      if (Settings.System.canWrite(InCallScreen.this)) {
        Settings.System.putInt(cr, Settings.System.HAPTIC_FEEDBACK_ENABLED, 0);
      }
    }
  }

  @Override
  public void onAnswer() {
    answer();
  }

  private void log(String msg) {
    Log.d(LOG_TAG, "[CallCard " + this + "] " + msg);
  }

  private void showCallConnecting() {
    if (DBG) log("showCallConnecting()...");
    // TODO: remove if truly unused
  }

  private void showCallIncoming() {
    if (DBG) log("showCallIncoming()...");
    // TODO: remove if truly unused
  }

  private void showCallConnected() {
    if (DBG) log("showCallConnected()...");
    // TODO: remove if truly unused
  }

  private void showCallEnded() {
    if (DBG) log("showCallEnded()...");
    // TODO: remove if truly unused
  }
  private void showCallOnhold() {
    if (DBG) log("showCallOnhold()...");
    // TODO: remove if truly unused
  }

  public void reset() {
    if (DBG) log("reset()...");

    // default to show ACTIVE call style, with empty title and status text
    showCallConnected();
//        mUpperTitle.setText("");
  }

  /**
   * Updates the main block of caller info on the CallCard
   * (ie. the stuff in the mainCallCard block) based on the specified Call.
   */
  public void displayMainCallStatus(Phone phone, Call call) {
    if (DBG) log("displayMainCallStatus(phone " + phone
            + ", call " + call + ", state" + call.getState() + ")...");

    Call.State state = call.getState();
    boolean landscapeMode = getResources().getConfiguration().orientation == Configuration.ORIENTATION_LANDSCAPE;

    switch (state) {
      case ACTIVE:
        showCallConnected();

        mIncallControlsContainerLl.setVisibility(View.VISIBLE);
        mIncallAnswerContainerLl.setVisibility(View.GONE);
        mBottomIncallAction.setVisibility(View.VISIBLE);

        // update timer field
        if (DBG) log("displayMainCallStatus: start periodicUpdateTimer");
        break;

      case HOLDING:
        showCallOnhold();

        break;

      case DISCONNECTED:
      case END_CALL:
        reset();
        showCallEnded();

        mIncallControlsContainerLl.setVisibility(View.GONE);
        mElapsedTimeTv.stop();

        new Handler().postDelayed(new Runnable() {
          @Override
          public void run() {
//            mIncallControlsContainerLl.setVisibility(View.GONE);
            finish();
          }
        }, 3000);

        break;

      case DIALING:
      case ALERTING:
        showCallConnecting();

        mIncallControlsContainerLl.setVisibility(View.VISIBLE);
        mIncallAnswerContainerLl.setVisibility(View.GONE);

        mBottomIncallAction.setVisibility(View.GONE);

        break;

      case INCOMING:
      case WAITING:
        showCallIncoming();
        mIncallControlsContainerLl.setVisibility(View.VISIBLE);
        mIncallAnswerContainerLl.setVisibility(View.GONE);

        break;

      case IDLE:
        // The "main CallCard" should never display an idle call!
        Log.w(LOG_TAG, "displayMainCallStatus: IDLE call in the main call card!");
        break;

      default:
        Log.w(LOG_TAG, "displayMainCallStatus: unexpected call state: " + state);
        break;
    }

    updateCardTitleWidgets(phone, call);
    {
      // Update onscreen info for a regular call (which presumably
      // has only one connection.)
      Connection conn = call.getEarliestConnection();

      boolean isPrivateNumber = false; // TODO: need isPrivate() API

      if (conn == null) {
        if (DBG) log("displayMainCallStatus: connection is null, using default values.");
        // if the connection is null, we run through the behaviour
        // we had in the past, which breaks down into trivial steps
        // with the current implementation of getCallerInfo and
        // updateDisplayForPerson.
//        updateDisplayForPerson(null, isPrivateNumber, false, call);
      } else {
        if (DBG) log("  - CONN: " + conn + ", state = " + conn.getState());

        // make sure that we only make a new query when the current
        // callerinfo differs from what we've been requested to display.
        boolean runQuery = true;
        Object o = conn.getUserData();
//        if (o instanceof PhoneUtils.CallerInfoToken) {
//          runQuery = mPhotoTracker.isDifferentImageRequest(
//                  ((PhoneUtils.CallerInfoToken) o).currentInfo);
//        } else {
//          runQuery = mPhotoTracker.isDifferentImageRequest(conn);
//        }

        if (runQuery) {
          if (DBG) log("- displayMainCallStatus: starting CallerInfo query...");
//          PhoneUtils.CallerInfoToken info =
//                  PhoneUtils.startGetCallerInfo(getContext(), conn, this, call);
//          updateDisplayForPerson(info.currentInfo, isPrivateNumber, !info.isFinal, call);
        } else {
          // No need to fire off a new query.  We do still need
          // to update the display, though (since we might have
          // previously been in the "conference call" state.)
          if (DBG) log("- displayMainCallStatus: using data we already have...");
          if (o instanceof CallerInfo) {
            CallerInfo ci = (CallerInfo) o;
            if (DBG) log("   ==> Got CallerInfo; updating display: ci = " + ci);
//            updateDisplayForPerson(ci, false, false, call);
          } else if (o instanceof PhoneUtils.CallerInfoToken){
            CallerInfo ci = ((PhoneUtils.CallerInfoToken) o).currentInfo;
            if (DBG) log("   ==> Got CallerInfoToken; updating display: ci = " + ci);
//            updateDisplayForPerson(ci, false, true, call);
          } else {
            Log.w(LOG_TAG, "displayMainCallStatus: runQuery was false, "
                    + "but we didn't have a cached CallerInfo object!  o = " + o);
            // TODO: any easy way to recover here (given that
            // the CallCard is probably displaying stale info
            // right now?)  Maybe force the CallCard into the
            // "Unknown" state?
          }
        }
      }
    }

    // In some states we override the "photo" ImageView to be an
    // indication of the current state, rather than displaying the
    // regular photo as set above.
//    updatePhotoForCallState(call);

    // Set the background frame color based on the state of the call.
//        setMainCallCardBackgroundResource(callCardBackgroundResid);
    // (Text colors are set in updateCardTitleWidgets().)
  }

  /**
   * Updates the "upper" and "lower" titles based on the current state of this call.
   */
  private void updateCardTitleWidgets(Phone phone, Call call) {
    if (DBG) log("updateCardTitleWidgets(call " + call + ")...");
    Call.State state = call.getState();

    // TODO: Still need clearer spec on exactly how title *and* status get
    // set in all states.  (Then, given that info, refactor the code
    // here to be more clear about exactly which widgets on the card
    // need to be set.)

    // Normal "foreground" call card:
    String cardTitle = getTitleForCallCard(call);

    if (DBG) log("updateCardTitleWidgets: " + cardTitle);

    // We display *either* the "upper title" or the "lower title", but
    // never both.
    if (state == Call.State.ACTIVE) {
//       Use the "lower title" (in green).
//            mLowerTitleViewGroup.setVisibility(View.VISIBLE);
//            mLowerTitleIcon.setImageResource(R.drawable.ic_incall_ongoing);
//            mLowerTitle.setText(cardTitle);
//            mLowerTitle.setTextColor(mTextColorConnected);
//      mElapsedTimeTv.setTextColor(mTextColorConnected);
      mElapsedTimeTv.setBase(call.base);
      mElapsedTimeTv.start();
      mElapsedTimeTv.setVisibility(View.VISIBLE);
//            mUpperTitle.setText("");
    } else if (state == Call.State.DISCONNECTED) {
      // Use the "lower title" (in red).
      // TODO: We may not *always* want to use the lower title for
      // the DISCONNECTED state.  "Error" states like BUSY or
      // CONGESTION (see getCallFailedString()) should probably go
      // in the upper title, for example.  In fact, the lower title
      // should probably be used *only* for the normal "Call ended"
      // case.
//            mLowerTitleViewGroup.setVisibility(View.VISIBLE);
//            mLowerTitleIcon.setImageResource(R.drawable.ic_incall_end);
//            mLowerTitle.setText(cardTitle);
//            mLowerTitle.setTextColor(mTextColorEnded);
//            mElapsedTime.setTextColor(mTextColorEnded);
            if (call.base != 0) {
	            mElapsedTimeTv.setBase(call.base);
              mElapsedTimeTv.start();
              mElapsedTimeTv.stop();
            } else
              mElapsedTimeTv.setVisibility(View.INVISIBLE);
//            mUpperTitle.setText("");
        } else {
//       All other states use the "upper title":
//            mUpperTitle.setText(cardTitle);
//            mLowerTitleViewGroup.setVisibility(View.INVISIBLE);
            if (state != Call.State.HOLDING)
              mElapsedTimeTv.setVisibility(View.INVISIBLE);
    }
  }

  /**
   * Returns the "card title" displayed at the top of a foreground
   * ("active") CallCard to indicate the current state of this call, like
   * "Dialing" or "In call" or "On hold".  A null return value means that
   * there's no title string for this state.
   */
  private String getTitleForCallCard(Call call) {
    String retVal = null;
    Call.State state = call.getState();
    Context context = this;

    if (DBG) log("- getTitleForCallCard(Call " + call + ")...");

    switch (state) {
      case IDLE:
        break;

      case ACTIVE:
        // Title is "Call in progress".  (Note this appears in the
        // "lower title" area of the CallCard.)
        retVal = context.getString(R.string.card_title_in_progress);
        break;

      case HOLDING:
        retVal = context.getString(R.string.card_title_on_hold);
        // TODO: if this is a conference call on hold,
        // maybe have a special title here too?
        break;

      case DIALING:
      case ALERTING:
        retVal = context.getString(R.string.card_title_dialing);
        break;

      case INCOMING:
      case WAITING:
        retVal = context.getString(R.string.card_title_incoming_call);
        break;

      case DISCONNECTED:
        retVal = getCallFailedString(call);
        break;
    }

    if (DBG) log("  ==> result: " + retVal);
    return retVal;
  }

  /**
   * Updates the "on hold" box in the "other call" info area
   * (ie. the stuff in the otherCallOnHoldInfo block)
   * based on the specified Call.
   * Or, clear out the "on hold" box if the specified call
   * is null or idle.
   */
  public void displayOnHoldCallStatus(Phone phone, Call call) {
    if (DBG) log("displayOnHoldCallStatus(call =" + call + ")...");
    if (call == null) {
//            mOtherCallOnHoldInfoArea.setVisibility(View.GONE);
      return;
    }

    Call.State state = call.getState();
    switch (state) {
      case HOLDING:
        // Ok, there actually is a background call on hold.
        // Display the "on hold" box.
        String name;

        // First, see if we need to query.
      {
        // perform query and update the name temporarily
        // make sure we hand the textview we want updated to the
        // callback function.
        if (DBG) log("==> NOT a conf call; call startGetCallerInfo...");
//                    PhoneUtils.CallerInfoToken info = PhoneUtils.startGetCallerInfo(
//                            getContext(), call, this, mOtherCallOnHoldName);
//                    name = PhoneUtils.getCompactNameFromCallerInfo(info.currentInfo, getContext());
      }

//                mOtherCallOnHoldName.setText(name);
//
//                // The call here is always "on hold", so use the orange "hold" frame
//                // and orange text color:
//                setOnHoldInfoAreaBackgroundResource(R.drawable.incall_frame_hold_short);
//                mOtherCallOnHoldName.setTextColor(mTextColorOnHold);
//                mOtherCallOnHoldStatus.setTextColor(mTextColorOnHold);
//
//                mOtherCallOnHoldInfoArea.setVisibility(View.VISIBLE);

      break;

      default:
        // There's actually no call on hold.  (Presumably this call's
        // state is IDLE, since any other state is meaningless for the
        // background call.)
//                mOtherCallOnHoldInfoArea.setVisibility(View.GONE);
        break;
    }
  }

  private String getCallFailedString(Call call) {
    int resID = R.string.card_title_call_ended;

    if (Receiver.call_end_reason != -1)
      resID = Receiver.call_end_reason;

    return getString(resID);
  }

  /**
   * Updates the "Ongoing call" box in the "other call" info area
   * (ie. the stuff in the otherCallOngoingInfo block)
   * based on the specified Call.
   * Or, clear out the "ongoing call" box if the specified call
   * is null or idle.
   */
  public void displayOngoingCallStatus(Phone phone, Call call) {
    if (DBG) log("displayOngoingCallStatus(call =" + call + ")...");
    if (call == null) {
//            mOtherCallOngoingInfoArea.setVisibility(View.GONE);
      return;
    }

    Call.State state = call.getState();
    switch (state) {
      case ACTIVE:
      case DIALING:
      case ALERTING:
        // Ok, there actually is an ongoing call.
        // Display the "ongoing call" box.
        String name;

        // First, see if we need to query.
      {
        // perform query and update the name temporarily
        // make sure we hand the textview we want updated to the
        // callback function.
//                    PhoneUtils.CallerInfoToken info = PhoneUtils.startGetCallerInfo(
//                            getContext(), call, this, mOtherCallOngoingName);
//                    name = PhoneUtils.getCompactNameFromCallerInfo(info.currentInfo, getContext());
      }

//                mOtherCallOngoingName.setText(name);
//
//                // The call here is always "ongoing", so use the green "connected" frame
//                // and green text color:
//                setOngoingInfoAreaBackgroundResource(R.drawable.incall_frame_connected_short);
//                mOtherCallOngoingName.setTextColor(mTextColorConnected);
//                mOtherCallOngoingStatus.setTextColor(mTextColorConnected);
//
//                mOtherCallOngoingInfoArea.setVisibility(View.VISIBLE);

      break;

      default:
        // There's actually no ongoing call.  (Presumably this call's
        // state is IDLE, since any other state is meaningless for the
        // foreground call.)
//                mOtherCallOngoingInfoArea.setVisibility(View.GONE);
        break;
    }
  }

  @Override
  public void onClick(View view) {
    if (view == mAnswerCallLl) {
      answer();
//      Receiver.engine(mContext).speaker(AudioManager.MODE_IN_CALL);

    } else if (view == mCancelCallLl) {
      isRejectCall = true;
      reject();
    } else if (view == mHoldingCallLl) {
//      Receiver.engine(mContext).togglehold();
    } else if (view == mVolumnLl) {
//      Receiver.engine(mContext).togglemute();
    } else if (view == mSpeakerLl) {
      showMessage();
//      Receiver.engine(mContext).speaker(RtpStreamReceiver.speakermode == AudioManager.MODE_NORMAL?
//              AudioManager.MODE_IN_COMMUNICATION : AudioManager.MODE_NORMAL);
    } else if (view == mEndCallRl) {
      isRejectCall = true;
      endCall();
    }
  }
}
